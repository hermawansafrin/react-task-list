import './App.css';
import logo from './logo.svg';

import FromInput from './components/FormInput';
import React from 'react';
import ToDoItem from './components/ToDoItem';
import EditModal from './components/EditModal';
import DeleteConfirmation from './components/DeleteConfirmation';

class App extends React.Component {
  state = {
    todos: [
      {
        id: 1,
        title: "To do title 1"
      },
      {
        id: 2,
        title: "To do title 2"
      },
      {
        id: 3,
        title: "To do title 3"
      },
    ],
    isEdit: false,
    editData: {
      id: "",
      title: ""
    },
    isDelete: false,
    deletedId: ""
  };

  deleteTask = () => {
    this.setState({
      todos: this.state.todos.filter(item => item.id !== this.state.deletedId),
      isDelete: false
    })
  };

  addTask = data => {
    console.log("Adding data");
    const id = this.state.todos.length
    const newData = {
      id: id + 1,
      title: data
    }
    this.setState({
      todos: [...this.state.todos, newData]
    })
  };

  handleOpenModal = (id, data) => {
    this.setState({
      isEdit: true,
      editData: {
        id : id,
        title:data
      }
    });
  };

  handleOpenDeleteModal = (id) => {
    this.setState({
      isDelete: true,
      deletedId: id
    })
  };

  handleCloseModal = () => {
    this.setState({isEdit:false});
  };

  handleCloseDeleteModal = () => {
    console.log('close modal delete');
    console.log("state : " + this.state.isDelete)
    this.setState({isDelete: false})
  };

  handleEditOnTitle = (event) => {
    this.setState({
      editData: {
        ...this.state.editData,
        title: event.target.value
      }
    })
  }

  handleOnUpdate = () => {
    const {id, title} = this.state.editData
    const newData = {id, title};

    const newTodos = this.state.todos;
    newTodos.splice((id-1), 1, newData);

    this.setState({
      todos : newTodos, 
      isEdit: false, 
      editData : {
        id: "",
        title: ""
      }
    })
  }

  render(){
    const {todos} = this.state;
    return (
      <div className="app">
        <div className='logo'>
          <img src={logo} alt='logo' />
          <h3>Task List Apps</h3>
        </div>
        <div className='list'>
          {todos.map(item => 
            <ToDoItem key={item.id} 
              todo={item} 
              del={this.deleteTask} 
              open={this.handleOpenModal}
              deleteConfirmation={this.handleOpenDeleteModal}
              />
            )
          }
        </div>
        
        <div className='input-form'>
          <FromInput 
            add={this.addTask}
            />
        </div>

        <EditModal
          isEdit={this.state.isEdit}
          close={this.handleCloseModal}
          change={this.handleEditOnTitle}
          data = {this.state.editData}
          update= {this.handleOnUpdate}
        />

        <DeleteConfirmation 
          isDelete={this.state.isDelete}
          actionClose={this.handleCloseDeleteModal}
          actionDelete={this.deleteTask}
        />

      </div>
    );
  }
  
}

export default App;
