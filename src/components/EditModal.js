import React from 'react';
import Button from './Button';
import "../styles/EditModal.css";

class EditModal extends React.Component{
    render(){
        //destructor
        const {isEdit, close, data, change, update} = this.props
        if(isEdit){
            return(
                <div className='modal-container'>
                    <div className='modal-box'>
                        <h3>Edit Task</h3>
                        <div className='input'>
                            <input type='text' 
                                value={data.title}
                                onChange={change}
                                />
                        </div>
                        <div className='btn-group'>
                            <Button 
                                text="Edit" 
                                variant='primary'
                                action={update}
                                />
                            <Button 
                                text="Close"
                                variant='danger'
                                action={close}
                                />
                        </div>
                    </div>
                </div>
            )
        }
        return null
    }
}

export default EditModal