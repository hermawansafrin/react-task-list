import React from "react";
import Button from "./Button";
import PropTypes from "prop-types";

const ToDoItem = ({todo, open, deleteConfirmation}) => {
    // function helper
    // const delById = (id) => {
    //     del(id)
    // }

    return (
        <div style={todoItemStyle}>
            <p>{todo.title}</p>
            <div>
                <Button text="Edit" 
                    variant="success"
                    action={() => open(todo.id, todo.title)}
                />
                <Button text="Delete" 
                    variant="danger" 
                    action={() => deleteConfirmation(todo.id)}
                    // action={() => delById(todo.id)}
                    />
            </div>
        </div>
    );
};

ToDoItem.prototypes = {
    todo: PropTypes.object.isRequired,
    del: PropTypes.func.isRequired
};

/** try with inline styles */
const todoItemStyle = {
    background: "#2DA4F8",
    color: "#FFF",
    display: "flex",
    alignItems: "center",
    height: "3rem",
    padding: "0 1rem",
    justifyContent: "space-between",
    margin: "0.5rem 0"
};

export default ToDoItem;