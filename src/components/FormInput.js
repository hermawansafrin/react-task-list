import React from "react"
import Button from "./Button";
import "../styles/FormInput.css";

/**
 * Using class component because in here using state
 */
class FromInput extends React.Component{
    state = {
        text: ""
    };
    
    handleChange = (event) => {
        this.setState({text: event.target.value})
    };

    handleSubmit = (event) => {
        event.preventDefault()
        if(this.state.text !== ""){
            //do submit with props from App.js
            this.props.add(this.state.text)
            this.setState({text: ""})
        }
    };


    render(){
        return(
            <form style={FromInputStyle} onSubmit={this.submit}>
                <input 
                    type="text" 
                    style={inputStyle}
                    placeholder="Add Task.."
                    onChange={this.handleChange}
                    value={this.state.text}
                    />
                <Button text="Add " variant="primary" action={this.handleSubmit}/>
            </form>
        )
    }
}

// try with inline style (same with ToDoItem)
const FromInputStyle = {
    background: "#fff",
    color: "#FFF",
    display: "flex",
    alignItems: "center",
    height: "3rem",
    padding: "0 1rem",
    justifyContent: "space-between",
    margin: "0.5rem 0"
};

const inputStyle = {
    width: "70%",
    border: "none"
};

export default FromInput;