import React from "react";
import PropTypes from "prop-types";
import '../styles/Button.css';

/**
 * Not using class component because there is no state here.
 * props here destructure
 */
const Button = ({variant, text, action}) => {
    return (
        <button className={`btn btn-${variant}`} 
            onClick={action}
            >
            {text}
        </button>
    );
};

Button.propTypes = {
    text: PropTypes.string.isRequired,
    variant: PropTypes.string.isRequired,
    action: PropTypes.func
}

export default  Button;