import React from "react";
import "../styles/EditModal.css";
import Button from "./Button";


class deleteConfirmation extends React.Component {
    render(){
        const {isDelete, actionDelete, actionClose} = this.props
        if(isDelete){
            return(
                <div className='modal-container'>
                    <div className='modal-box'>
                        <h3>Are you sure ?</h3>
                        <br/>
                        <div className='btn-group'>
                            <Button 
                                text="Delete"
                                variant="primary"
                                action={actionDelete}
                            />
                            <Button
                                text="Close"
                                variant="danger"
                                action={actionClose}
                            />
                            {/* <Button 
                                text="Edit" 
                                variant='primary'
                                action={update}
                                />
                            <Button 
                                text="Close"
                                variant='danger'
                                action={close}
                                /> */}
                        </div>
                    </div>
                </div>
            )
        }

        return null
    }
}

export default deleteConfirmation;